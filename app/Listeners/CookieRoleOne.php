<?php

namespace App\Listeners;

use App\Models\User;
use Illuminate\Auth\Events\Login;

class CookieRoleOne
{
    /**
     * Handle the event.
     *
     * @param Login $event
     * @return void
     */
    public function handle(Login $event)
    {
        //Get env variable COOKIE_ROLE
        $cookie_role = env('COOKIE_ROLE', '1');
        //Get env variable COOKIE_IP
        $cookie_ip = env('COOKIE_IP', '127.0.0.1');
        // Create a cookie with name origin_sesion when user has role = $cookie_rol and ip is $cookie_ip
        if ($event->user->role_id == $cookie_role && request()->ip() == $cookie_ip) {
            // Create a cookie with name origin_sesion with values $cookie_role and $cookie_ip
            cookie()->queue(cookie('origin_sesion', $cookie_role . '|' . $cookie_ip, 60));
        }
    }
}
