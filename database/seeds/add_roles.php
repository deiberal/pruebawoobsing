<?php

use App\Role;
use Illuminate\Database\Seeder;

class add_roles extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Create a role with the name user
        $role = new Role();
        $role->name = 'user';
        $role->save();

    }
}
