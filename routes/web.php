<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
//Verified the auth routes
Auth::routes(['verify' => true]);
//Email Verify
Route::get('/verificacion', function () {
    return view('auth.verify');
})->middleware('auth')->name('verification.notice');

Route::group(['middleware' => ['auth', 'verified']], function () {
    Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
    Route::get('/sesiones', [App\Http\Controllers\HomeController::class, 'sesiones'])->name('sesiones');
});
//Two Factor
Route::post('/login-two-factor/{user}', 'Auth\LoginController@login2FA')->name('login.2fa');

